<div class="mb-3 row">
    <label for="{{ $id }}" class="col-sm-4 col-form-label">{{ $label }}</label>
    <div class="col-sm-8 form-switch">
        &nbsp;&nbsp;
        <input type="checkbox"
               id="{{ $id }}"
               name="{{ $id }}"
               @if($value) checked @endif
               {{ $attributes->merge(['class' => 'form-check-input']) }}>
    </div>
</div>