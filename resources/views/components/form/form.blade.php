<form method="{{ $method ?? 'post' }}" action="{{ $action }}">
    @csrf
    @if ($redirect ?? false)
        <input type="hidden" name="redirect" value="{{ $redirect }}">
    @endif
    {{ $slot }}
</form>