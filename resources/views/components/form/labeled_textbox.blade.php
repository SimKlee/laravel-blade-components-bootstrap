<div class="mb-3 row">
    <label for="{{ $id }}" class="col-sm-4 col-form-label">{{ $label }}</label>
    <div class="col-sm-8">
        <input type="{{ $type ?? 'text' }}"
               id="{{ $id }}"
               name="{{ $id }}"
               value="{{ $value ?? null }}"
               {{ $attributes->merge(['class' => 'form-control']) }}>
    </div>
</div>