<x-lbcb::card.card>
    <x-lbcb::card.header>
        @if($actions ?? false)
            <div class="d-flex col-12">
                <div class="d-flex col-6 justify-content-start"><h3>{{ $title }}</h3></div>
                <div class="d-flex col-6 justify-content-end">{{ $actions }}</div>
            </div>
        @else
            <h3>{{ $title }}</h3>
        @endif
    </x-lbcb::card.header>
    <x-lbcb::card.body>
        @if($responsive ?? true)
        <div class="table-responsive">
        @endif
            <table class="table table-sm table-bordered table-hover table-striped">
                @if($thead ?? false)
                    <x-lbcb::table.header>
                        {{ $thead }}
                    </x-lbcb::table.header>
                @endif
                <tbody>
                {{ $tbody ?? null }}
                </tbody>
            </table>
        @if($responsive ?? true)
        </div>
        @endif
    </x-lbcb::card.body>
    <x-lbcb::card.footer>
        {{ $paginator->links() }}
    </x-lbcb::card.footer>
</x-lbcb::card.card>