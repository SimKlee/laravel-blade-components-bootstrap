<x-lbcb::card.card>
    <x-lbcb::card.header><h3>{{ $title }}</h3></x-lbcb::card.header>
    <x-lbcb::card.body>
        <table class="table table-bordered table-hover table-striped">
            @if($thead ?? false)
                <x-table.header>
                    {{ $thead }}
                </x-table.header>
            @endif
            <tbody>
            {{ $tbody ?? null }}
            </tbody>
        </table>
    </x-lbcb::card.body>
</x-lbcb::card.card>