<table class="table table-bordered table-hover table-striped">
    @if($thead ?? false)
        <x-lbcb::table.header>
            {{ $thead }}
        </x-lbcb::table.header>
    @endif
    <tbody>
    {{ $tbody ?? null }}
    </tbody>
</table>