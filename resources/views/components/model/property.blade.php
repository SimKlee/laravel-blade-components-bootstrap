@switch($type)
    @default
    <x-lbcb::form.labeled_textbox id="{{ $property }}"
                                  label="{{ $label ?? '' }}"
                                  value="{{ $value ?? '' }}"/>
@endswitch