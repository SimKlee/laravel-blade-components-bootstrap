<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponentsBootstrap;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class LaravelBladeComponentsBootstrapServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Paginator::useBootstrap();

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'lbcb');
        $this->loadViewComponentsAs('lbcb', $this->getBladeComponents());
    }

    public function register(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../src/View/Components/'        => app_path('View/Components'),
                __DIR__ . '/../resources/views/components/' => resource_path('views/components'),
            ], 'view-components');
        }
    }

    private function getBladeComponents(): array
    {
        return [
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Card\Card::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Card\Header::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Card\Body::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Card\Footer::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\Table::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\CardTable::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\CardDataTable::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\Cell::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\Header::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\HeaderItem::class,
            \SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table\Row::class,
        ];
    }

}