<?php

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\Component;

class CardDataTable extends Component
{
    public string                $title;
    public ?LengthAwarePaginator $paginator = null;
    public bool                  $responsive;

    public function __construct(string $title, LengthAwarePaginator $paginator = null, bool $responsive = true)
    {
        $this->title      = $title;
        $this->paginator  = $paginator;
        $this->responsive = $responsive;
    }

    public function render(): View|Closure|string
    {
        return view('lbcb::components.table.card-data-table');
    }
}
