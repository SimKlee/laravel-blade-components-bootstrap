<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Cell extends Component
{
    public function __construct()
    {

    }

    public function render(): View|Closure|string
    {
        return view('lbcb::components.table.cell');
    }
}
