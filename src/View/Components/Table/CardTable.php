<?php

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Table;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\Component;

class CardTable extends Component
{
    public string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function render(): View|Closure|string
    {
        return view('lbcb::components.table.card-table');
    }
}
