<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Model;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class Property extends Component
{
    public Model|string $model;
    public string       $property;
    public string       $type;
    public string       $label;
    public string|null  $value;

    public function __construct(Model|string $model, string $property, string $type = 'string', string $label = null, mixed $value = null)
    {
        $this->model    = $model;
        $this->property = $property;
        $this->type     = $type;
        $this->label    = $label ?? $this->getTranslatedLabel();
        $this->value    = $this->model instanceof Model ? $this->model->{$this->property} : $value;
    }

    public function render(): View|Closure|string
    {
        return view('lbcb::components.model.property')
            ->with('translated', $this->getTranslatedLabel());
    }

    public function getTranslatedLabel(): string
    {
        return trans(sprintf('%s.columns.%s', Str::lower(Str::snake($this->model)), $this->property));
    }
}
