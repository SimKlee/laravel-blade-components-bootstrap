<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Form extends Component
{
    public string $action;
    public string $method;
    public string $redirect;
    public bool   $multiform; // @TODO: implement in template

    public function __construct(string $action, string $method, string $redirect, bool $multiform = false)
    {
        $this->action    = $action;
        $this->method    = $method;
        $this->redirect  = $redirect;
        $this->multiform = $multiform;
    }

    public function render(): View|Closure|string
    {
        return view('lbcb::components.form.form');
    }
}
