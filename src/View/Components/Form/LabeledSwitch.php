<?php

declare(strict_types=1);

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Form;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class LabeledSwitch extends Component
{
    public string $id;
    public string $label;
    public mixed $value;

    public function __construct(string $id, string $label, mixed $value = null)
    {
        $this->id    = $id;
        $this->label = $label;
        $this->value = $value;
    }

    public function render(): View|Closure|string
    {
        return view('lbcb::components.form.labeled_switch');
    }
}
