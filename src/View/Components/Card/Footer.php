<?php

namespace SimKlee\LaravelBladeComponentsBootstrap\View\Components\Card;

use Illuminate\View\Component;

class Footer extends Component
{
    public function __construct()
    {

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('lbcb::components.card.footer');
    }
}
